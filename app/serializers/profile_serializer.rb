class ProfileSerializer < ActiveModel::Serializer
  attributes :id, :firstname, :lastname
end
