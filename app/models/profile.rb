class Profile < ApplicationRecord
  belongs_to :user
  has_many :hobbies
  has_many :photos
  has_many :certificates
  has_many :friends

  attr_accessor :json

  def build_json
    @json = {}
    self.attributes.each { |key, value| @json[key] = value }
    @json['email'] = self.user.email

    get_hobbies
    get_certificates

    @json
  end

  def get_hobbies
    @json['hobbies'] = []
    self.hobbies.each do |hobby|
      hobby_hash = hobby.attributes
      hobby_hash['category'] = hobby.category.name
      @json['hobbies'] <<  hobby_hash
    end
  end

  def get_certificates
    @json['certificates'] = []
    self.certificates.each { |certificate| @json['certificates'] << certificate.attributes }
  end

end
