
class BrowseController < ApplicationController
  before_action :set_profile
  #before_action :set_age, :only => [:browsedprofile]
  skip_before_action :verify_authenticity_token, :only => [:get_response]

  def search
  end

  def get_response
    url = params[:url]
    response = HTTParty.get(url)
    browsedprofile(response, url)
  end

  def browsedprofile(response, url)
    @response = response
    @url = url
    @age = set_age(@response['birthdate'].to_time)
    @profile = set_profile
    render 'browsedprofile'
  end

  private

  def set_profile
    @profile = Profile.find(1)
  end

  def set_age b
    now = Time.now
    now.year - b.year - ((now.month > b.month || (now.month == b.month && now.day >= b.day)) ? 0 : 1)
  end

end
