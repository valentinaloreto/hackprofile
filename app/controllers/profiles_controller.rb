class ProfilesController < ApplicationController
  before_action :set_profile, except: :json_response
  before_action :set_age, except: :json_response
  before_action :set_hobbies, except: :json_response
  before_action :set_photos, except: :json_response
  before_action :set_friends, except: :json_response
  before_action :set_certificates, except: :json_response

  def show
  end

  def edit
  end

  def update
    if @profile.update(profile_params)
      redirect_to @profile, notice: 'Your profile was successfully updated.'
    else
      render :edit
    end
  end

  def json_response
    profile = Profile.first
    json = profile.build_json
    photo = profile.photos.first.photo_file
    json['image'] = url_for(photo)
    render json: json
  end

  private

  def set_profile
    @profile = Profile.find(params[:id])
  end

  def set_hobbies
    @hobbies = @profile.hobbies
  end

  def set_photos
    @photos = @profile.photos
  end

  def set_certificates
    @certificates = @profile.certificates
  end

  def set_friends
    @friends = @profile.friends
  end

  def set_age
    now = Time.now
    b = @profile.birthdate.to_time
    @age = now.year - b.year - ((now.month > b.month || (now.month == b.month && now.day >= b.day)) ? 0 : 1)
  end

  def profile_params
    params.require(:profile).permit(:firstname, :lastname, :username, :bio, :birthdate, :subtitle)
  end

end
