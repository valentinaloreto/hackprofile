class CertificatesController < ApplicationController
  before_action :set_profile, only: [:index, :new, :create]
  before_action :set_certificate, only: [:show, :edit, :update, :destroy]

  def index
    @certificates = Certificate.all
  end

  def new
    @certificate = Certificate.new
  end

  def create
    @certificate = @profile.certificates.build(certificate_params)
    if @certificate.save
      redirect_to profile_certificates_path(@profile), notice: 'certificate was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @certificate.update(certificate_params)
      redirect_to profile_certificates_path(@certificate.profile.id), notice: 'certificate was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @profile = @certificate.profile.id
    @certificate.destroy
    redirect_to profile_certificates_path(@profile), notice: 'certificate was successfully destroyed.'
  end

  private

  def set_certificate
    @certificate = Certificate.find(params[:id])
  end

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def certificate_params
    params.require(:certificate).permit(:name, :file)
  end
end
