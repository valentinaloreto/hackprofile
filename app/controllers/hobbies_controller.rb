class HobbiesController < ApplicationController
  before_action :set_profile, only: [:index, :new, :create]
  before_action :set_hobby, only: [:show, :edit, :update, :destroy]
  before_action :set_categories, only: [:new, :edit]
  before_action :set_preferences, only: [:new, :edit]

  def index
    @hobbies = Hobby.all.includes(:category)
  end

  def show
  end

  def new
    @hobby = Hobby.new
  end

  def create

    @hobby = @profile.hobbies.build(hobby_params)
    if @hobby.save
      redirect_to profile_hobbies_path, notice: 'Hobbie was successfully created.'
    else
      render :new
    end

  end

  def edit

  end

  def update
    if @hobby.update(hobby_params)
      redirect_to @hobby, notice: 'Your hobby was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @profile = @hobby.profile.id
    @hobby.destroy
    redirect_to profile_hobbies_path(@profile), notice: 'Hobby was successfully destroyed.'
  end

  private

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def set_hobby
    @hobby = Hobby.find(params[:id])
  end

  def set_categories
    @categories = Category.all.collect { |category| [category.name, category.id] }
  end

  def set_preferences
    @preferences = Hobby.preferences.collect { |key, value| [key.humanize, key] }
  end

  def hobby_params
    params.require(:hobby).permit(:name, :description, :preference, :category_id, :profile_id)
  end
end
