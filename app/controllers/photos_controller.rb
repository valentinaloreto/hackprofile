class PhotosController < ApplicationController
  before_action :set_profile, only: [:index, :new, :create]
  before_action :set_photo, only: [:show, :edit, :update, :destroy]

  def index
    @photos = Photo.all
  end

  def show
  end

  def new
    @photo = Photo.new
  end

  def create
    @photo = @profile.photos.build(photo_params)
    if @photo.save
      redirect_to profile_photos_path(@profile), notice: 'Photo was successfully created.'
    else
      render :new
    end
  end

  def edit

  end

  def update
    if @photo.update(photo_params)
      redirect_to @photo, notice: 'Your photo was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @profile = @photo.profile.id
    @photo.destroy
    redirect_to profile_photos_path(@profile), notice: 'Photo was successfully destroyed.'
  end

  private

  def set_photo
    @photo = Photo.find(params[:id])
  end

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def photo_params
    params.require(:photo).permit(:description, :photo_file, :profile_id)
  end
end
